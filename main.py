from __future__ import print_function

import math

import numpy as np

import librosa
import pygame
import random
from pygame.locals import *

milDelta = 0

class MusicBar:

    def __init__(self, x, y, width, maxHeight, color, freq_range, arr, angle):
        self.x = x
        self.y = y
        self.width = width
        self.maxHeight = maxHeight
        self.color = color
        self.height = 0
        self.freq_range = freq_range
        self.arr = arr
        self.angle = angle

    def update(self, time):
        max_freq = 0
        size = 0

        for f in self.freq_range:
            size = (80 - abs(self.arr[int(f / 10)][int(time / milDelta)])) * self.maxHeight/80

            max_freq = max(max_freq, size)

        size = max_freq + 120
        # size = 0.16 * max_freq
        # # size = max(0, size)
        # size = min(1, size)
        diff = size - self.height

        if abs(diff) > 20:
            vel = (size - self.height) / 10

            if 0 < vel < 3:
                vel = 3
            if -3 < vel < 0:
                vel = -3
        else:
            vel = 0

        # if height > size*300:
        #     height -= vel
        # elif height < size*300:
        self.height += vel

        self.height = min(self.maxHeight + 120, self.height)

    def render(self, screen):
        pygame.draw.line(screen, self.color, (600, 450), (600 + math.cos(math.radians(self.angle))*self.height, 450 + self.height*math.sin(math.radians(self.angle))), 12)
        # pygame.draw.rect(screen, self.color, (self.x, self.y - self.height, self.width, self.height))





def main():
    global milDelta
    filename = "music2.wav"
    # y, sr = librosa.load(filename)
    # tempo, beats = librosa.beat.beat_track(y=y, sr=sr)
    # beats_time = librosa.frames_to_time(beats)

    # y, sr = librosa.load(filename)
    # time = np.arange(0, len(y)) / sr
    # count = 0

    y, sr = librosa.load(filename)
    buffer = 2048
    # sr = 22050 #default sampling rate
    D = librosa.amplitude_to_db(librosa.stft(y), ref=np.max)

    # yaxis
    n = D.shape[0]
    yout = librosa.fft_frequencies(sr=sr, n_fft=1 + (2 * (n - 1)))
    # print (yout, yout.min(), yout.max())

    # xaxis
    m = D.shape[1]
    hop_length = 512

    xout = librosa.frames_to_time(np.arange(m + 1), sr=sr, hop_length=hop_length)
    print(len(yout))


    milDelta = (xout[len(xout) - 1]/len(xout)) * 1000
    hzDelta = 10

    # print()

    # Initialise screen
    pygame.init()
    screen = pygame.display.set_mode((1200, 900))
    pygame.display.set_caption('Basic Pygame program')

    # Fill background
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((0, 0, 0))

    pygame.font.init()  # you have to call this at the start,
    # if you want to use this module.
    myfont = pygame.font.SysFont('Comic Sans MS', 30)

    clock = pygame.time.Clock()

    # Blit everything to the screen
    screen.blit(background, (0, 0))
    pygame.display.flip()



    # lastHeight = 0

    max_size = 500
    bars = []

    startX = 20

    freq_range = [
        [40, 60],
        [61, 80],
        [81, 100],
        [101, 120],
        [121, 140],
        [141, 160],
        [161, 180],
        [181, 200],
        [201, 220],
        [221, 240],
        [241, 260],
        [261, 280],
        [281, 300],
        [301, 350],
        [351, 400],
        [401, 450],
        [451, 500],
        [501, 550],
        [551, 600],
        [601, 650],
        [651, 700],
        [701, 800],
        [801, 900],
        [901, 1000],
        [1001, 1300],
        [1301, 1600],
        [1601, 1900],
        [1901, 2300],
        [2301, 2900],
        [2901, 3900],
        [3901, 5000],
        [5001, 6000],
        [6001, 7000],
        [7001, 8000],
        [8001, 9001]

    ]

    angle_delta = 360.0/len(freq_range)
    i = 0.0

    print(angle_delta)



    width = 20

    for x in freq_range:
        bars.append(MusicBar(startX, 280, width, max_size, (random.randrange(255), 255, 255), x, D, angle_delta * i))
        startX += width + 2
        i += 1.0

    # bar = MusicBar(200, 550, 80, max_size, (255, 0, 255), 4096, D)
    # bar2 = MusicBar(360, 550, 80, max_size, (255, 255, 255), 2048, D)
    # bar3 = MusicBar(520, 550, 80, max_size, (255, 255, 0), 1024, D)

    # bars = [bar, bar2, bar3]
    pygame.mixer.music.load(filename)
    pygame.mixer.music.play(0, 0.0)
    time_start = pygame.time.get_ticks()

    # Event loop
    while 1:


        for event in pygame.event.get():
            if event.type == QUIT:
                return

        screen.blit(background, (0, 0))

        # size = (80 - abs(D[int(6000 / hzDelta)][int((pygame.time.get_ticks() - time_start) / milDelta)])) / 80
        # # size = max(0, size)
        # size = min(1, size)
        time = pygame.mixer.music.get_pos()
        for b in bars:
            b.update(time)
            b.render(screen)

        pygame.draw.circle(screen, (170, 170, 170), (600, 450), 120)
        pygame.draw.circle(screen, (190, 190, 190), (600, 450), 110)
        pygame.draw.circle(screen, (255, 255, 255), (600, 450), 100)



        # sec = int(pygame.mixer.music.get_pos()/1000)
        # minute = int(sec/60)
        # sec = sec%60
        #
        # minute = str(minute)
        # sec = "0" + str(sec) if sec < 10 else str(sec)
        #
        # textsurface = myfont.render(minute + ":" + sec, False, (255, 255, 255))
        # screen.blit(textsurface, (70, 500))

        pygame.display.flip()

        # pygame.time.delay(60)

if __name__ == '__main__': main()
