# import numpy as np
# import matplotlib.pyplot as plt
#
# import librosa
#
# filename = "music.wav"
# y, sr = librosa.load(filename)
#
# time = np.arange(0, len(y))/sr
# print(len(y))
# print(len(time))
# # print(time[23423])
# # print(y[23423])
# print(np.interp(1, time, y))
#
# fig, ax = plt.subplots()
#
# ax.plot(time, y)
#
# ax.set(xlabel='Time (s)', ylabel='amplitude')
#
# plt.show()
import scipy.io.wavfile

import numpy as np
import matplotlib.pyplot as plt

# file = os.path.join(temp_folder, file_name)

rate, aud_data = scipy.io.wavfile.read("music.wav")
# rate = 44000
ii = np.arange(0, 9218368)
t = ii / rate
# aud_data = np.zeros(len(t))
for w in [1000, 5000, 10000, 15000]:
    aud_data += np.cos(2 * np.pi * w * t)

# From here down, everything else can be the same
len_data = len(aud_data)

channel_1 = np.zeros(2**(int(np.ceil(np.log2(len_data)))))
channel_1[0:len_data] = aud_data

fourier = np.fft.fft(channel_1)
w = np.linspace(0, 44000, len(fourier))

# First half is the real component, second half is imaginary
fourier_to_plot = fourier[0:len(fourier)//2]
w = w[0:len(fourier)//2]

plt.figure(1)

plt.plot(w, fourier_to_plot)
plt.xlabel('frequency')
plt.ylabel('amplitude')
plt.show()



