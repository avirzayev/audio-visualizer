import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np

y, sr = librosa.load("music.wav")
#sr = 22050 #default sampling rate
D = librosa.amplitude_to_db(librosa.stft(y), ref=np.max)

# yaxis
n = D.shape[0]
yout = librosa.fft_frequencies(sr=sr, n_fft=1+(2 * (n - 1)) )
# print (yout, yout.min(), yout.max())

#xaxis
m = D.shape[1]
hop_length=512

xout = librosa.frames_to_time(np.arange(m+1), sr=sr, hop_length=hop_length)
print (len(xout))

milDelta = 43
hzDelta = 10

print(xout[len(xout) - 1])


librosa.display.specshow(D, sr=sr, hop_length=hop_length, y_axis='linear', x_axis='time')
plt.colorbar(format='%+2.0f dB')
plt.title('Log-frequency power spectrogram')
plt.savefig('sp.png')
plt.show()